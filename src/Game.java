/* Class Game
 * 
 * This handles the functionality of the game state
 * creates the grid and hides the treasure
 * checks moves
 */


import java.io.*;
import java.util.*;




class Game{
    
   /* Set the defaults for the game
    * These values are used by loadConfig() ONLY when no config file exists
    *  
    */
    
    private int size_of_grid = 3;
    private int difficulty_level =1;

    // Setting this to 1 here as opposed to in the 'config/TreasureHuntGameX.conf' file allows a 'debug by default;
    // version to be compiled.This is particularly useful for debugging the methods that load and save the config 
    // file as their functionality includes creating a config file.
    // Whilst intended for developer use only, these options remain here to permit academic assessment of their functionality.
    // For this purpose logging has been left enabled and debug output to the console disabled. 
    // This feature would be disabled or removed completely in a version that were made widely available.
    
    private int debug_mode = 0;
    
    // When set to 1 (default) the game will log debug messages to '/logs/TreasureHuntGameX.log
    private int logging_enabled = 1;
    
   
    private int moves_remaining;
    private int[][] treasure_array;
    private int no_of_quicksands;
    private int moves_allowed;
    private int search_area;

    private File config_file = new File("config/TreasureHuntGameX.conf");
    private File log_file = new File("logs/TreasureHuntGameX.log");

    /* Constructor for Game
     *
     * Initialises the game state
     *
     */

    public Game(){


        // Read from (or create) the config file with the Game.loadConfig() method

        loadConfig();

        // DEBUG OUTPUT    

        debugOut("Difficulty modifier set to " +difficulty_level );

        // Array to hold the contents of each grid square. 

        treasure_array = new int[size_of_grid][size_of_grid];
   
        // The number of 'hazards' that are generated and placed in the grid.
        // This is done before generating the treasure location so that the
        // treasure isn't overwritten by a hazard. This serves to further radnomise
        // gameplay as occasionally there will be one less hazard than the game rules 
        // would otherwise dictate. 
        // n.b. It is only at higher map sizes that these cease to be ourely quicksand hazards 
        // and begin to include drowning to allow for the edge of the map, which is mainly water,
        // to return a drowning hazard. 
       
        no_of_quicksands = ( ( size_of_grid / 2 ) + ( 1 / 2 ))*difficulty_level;

        // DEBUG OUTPUT - enabled by "debug_mode=1" in 'config/TreasureHuntGameX.conf'
        
        debugOut("Game object configured with " + no_of_quicksands + " quicksand patch(es)");

        // Conceal the treasure using our method
        
        hideTheTreasure(size_of_grid);

        // Calculate the number of moves for the current game size
        
        moves_allowed = ( ( size_of_grid * size_of_grid / 2 ) + 1 );
        
        
        // DEBUG OUTPUT - enabled by "debug_mode=1" in 'config/TreasureHuntGameX.conf'

        debugOut("Game object configured to allow " + moves_allowed + " moves");
        
        // Calculate the number of moves by dividing the moves calculated for the grid size
        // by the difficulty level (so half the moves at level 2 and a third at level 3). 
        // One is then added to keep things realistic at higher difficulty settings.
        
        moves_remaining = ( moves_allowed / difficulty_level ) + 1;
        
        // Calculate how many squares away the proximityTest method will search in using this formula
        // which was reached after careful experimentation to find the best spread of values
        // These could be hard coded, but this would mean that changes to the code to allow a different 
        // range of possible grid size and difficulty would require additional changes to this section
        //
        //    s-1
        //    --- + 0.5   
        //      2         = search area
        //    ----------
        //             d
        //
        //  Where s is the size of the grid and d is the difficulty level
        
        search_area = ( ( ( ( size_of_grid - 1 ) / 2 ) + ( 1 / 2 ) ) / difficulty_level );

        // DEBUG OUTPUT - enabled by "debug_mode=1" in 'config/TreasureHuntGameX.conf'

        debugOut("Grid size: " + size_of_grid + " Setting search area to: " + search_area);

   
      }
      
    /* Get the search area parameter.
     *
     * @return  int  A value the represents how many surrounding squares will be searched by the proximityTest method.
     *
     */

   public int getSearchArea(){
      
        return search_area;

   }

    /* Get the grid size value.
     *
     * @return  int  A value the represents how many squares across and deep the game grid will be 
     *
     */

   public int getGridSize(){
      
        return size_of_grid;

   }

    /* Set the size_of_grid value
     * Accepts a single int value
     * @return void
     *
     */
   
   public void setGridSize(int n){
       
       
       size_of_grid = n;
       
   }

    /* Get the difficulty level value
     *
     * @return  int  A value the represents how many surrounding squares will be searched by the proximityTest method.
     *
     */
  
   public int getDifficultyLevel(){
       
       return difficulty_level;
       
   }

    /* Set the search area parameter.
     *
     * @return  void  
     *
     */
   
   public void setDifficultyLevel(int n){
       
       difficulty_level = n;
   }



    /* Get the search area parameter.
     *
     * @return  int  A value the represents how many surrounding squares will be searched by the proximityTest method.
     *
     */
   
   public int getMovesRemaining(){
       
       return moves_remaining;
       
   }
   



    /* Decrements the moves_remaining variable by one
     *
     *
     */

    public void decrementMovesRemaining(){
   
        moves_remaining--;
   
        // trigger the insult if moves are low enough
        if(moves_remaining<moves_allowed/size_of_grid*0.5){
            insultMe();
        }
   
        // DEBUG FEEDBACK
        debugOut ("Moves remaining:" + getMovesRemaining());
   
    }




    /* Decrements the moves_remaining variable by a specified no of moves
     */

    public void decrementMovesRemaining(int no_to_remove){
   
        moves_remaining -= no_to_remove;
   
        // DEBUG FEEDBACK
        debugOut ("Moves remaining:" + getMovesRemaining());
   
    }




    /* Returns the number if moves that remain
     *
     */


    public int getMovesAllowed(){
   
        return moves_allowed;
   
    }



    /* Generates console output when debug_mode is true
    * Logs to file when logging_enabled is true
    * Allows all of the messages to be hidden in the final version
    *
    * @param debug_message The message to be logged
    * @return void The messages are logged, nothing is returned
    */

    public void debugOut(String debug_message){
   
        if(debug_mode==1){
            
            System.out.println("DEBUG: " + debug_message);

            // feedback_box.append("DEBUG: " + debug_message + "\n");
      
        }
   
        if(logging_enabled == 1 ){

            PrintWriter prtf = null;
            Date timestamp = new Date();
            

            try{
   
                // Create PrintWriter buffer to write to log file. true must be passed to  FileWriter to append to the file.

                prtf = new PrintWriter(new BufferedWriter( new FileWriter( log_file, true )));

                // Put the application name in parenthesis so that the log colouring tool grc  (used by the developer)
                // see [ http://kassiopeia.juls.savba.sk/~garabik/software/grc.html ]
                // can colour the application name accordingly making for more readable debug output to file

                prtf.println(timestamp.toString() + " (TreaureHuntGameX) : " + debug_message);
            
            }
            catch(IOException e) {
                e.printStackTrace();
     
            }

            finally{

                prtf.close();

            }
        
        }
   
   
    }




    /* Marks the passed grid square as having been searched
     *
     * Useful for console based implementations
     *
     */


    private void markSquareAsSearched(int move_x, int move_y){
   
      
        treasure_array[move_x][move_y] = 5;
   
   
    }




    /* Loads Game configuration from a File
     * If the file does not exist then one is created with the 
     * defaults at the beginning of this class
     * 
     * Modified from example at http://www.mkyong.com/java/java-properties-file-examples/
     */


    public void loadConfig(){


        Properties game_property = new Properties();

        if (( ! config_file.isFile()  ) || (! config_file.canRead() )){
      
            // if there is no config file available write a default config file
            debugOut("loadConfig: No config file found. Creating one.");
            saveConfig();

        }
        else{
   
            try{


                // create input stream

                FileReader reader = new FileReader(config_file); 
      
                // Create buffer object
          
                BufferedReader buffer_in = new BufferedReader(reader);

                // Read properties from the buffer
      
                game_property.load(buffer_in);
   
                // config values are stored as strings so need converting to int
                difficulty_level = Integer.parseInt(game_property.getProperty("difficulty"));
                size_of_grid = Integer.parseInt(game_property.getProperty("game_size"));
                debug_mode = Integer.parseInt(game_property.getProperty("debug_mode"));
                logging_enabled = Integer.parseInt(game_property.getProperty("logging_enabled"));
                debugOut("Config loaded successfully");

                
            }
            catch(IOException e) {

                // We already check the file's existance and readability abovr so
                // reaching this exception would indicate a more complex issue that 
                // wasn't already trapped.
                debugOut("loadConfig() Error. Stack Trace Follows");
                e.printStackTrace();
   
   
            }


        }
    }
    
   /* Config file is written every time a game option is changed
    * so that the game returns to it's last confniguration
    * the next time it is launched.
    *
    * Modified from example at http://www.mkyong.com/java/java-properties-file-examples/
    */

    public void saveConfig(){

        Properties game_property = new Properties();
   
        try{
   
            // Save current Settings to the config file
            game_property.setProperty("difficulty", Integer.toString(difficulty_level));
            game_property.setProperty("game_size", Integer.toString(size_of_grid));
            game_property.setProperty("debug_mode", Integer.toString(debug_mode));
            game_property.setProperty("logging_enabled", Integer.toString(logging_enabled));
         
      
            // Create buffer to write to config file.
            BufferedWriter buffer_out = new BufferedWriter( new FileWriter( config_file));
            game_property.store(buffer_out, null);
            debugOut("Config saved");
            
        }
        catch(IOException e) {
            e.printStackTrace();
   
   
        }
   


    }





    /* checkmove : a GET Method of class Game
     *
     * Accepts two int values that represent the chosen move which are checked in the treasure_array for the value
     *
     * If the treasure is found then
     *
     *
     * If quicksand is found then ladyLuck
     * 
     * @param move_x the current move's x coord
     * @param move_y the current move's y coord
     *
     * @return hint
     */

    public int checkMove(int move_x,int move_y){

        // Move result codes are:
        //
        // 0 : treasure,
        // 1 : death by quicksand,
        // 2 : nothing found,
        // 3 : escape from quicksand
        // 4 : out of moves;
        // 5 : already searched
        
        int move_result = 0; 
        // DEBUG FEEDBACK
        debugOut("Testing with move:"+ move_x + "," + move_y);
   
        switch (treasure_array[move_x][move_y]){



            case 1: // Treasure found
            
                // DEBUG FEEDBACK
                debugOut("You found the treasure ");


            break;



            case 0:  // Nothing was found

                // DEBUG FEEDBACK
                debugOut("There was nothing found.");
                move_result = 2;

            break;



            case 2: // Fell in quicksand

                // DEBUG FEEDBACK
                debugOut("Fell in quicksand");

                if(ladyLuck()==1){
                    debugOut("Perished in the quicksand");
                    move_result = 1;
                    break;
                }

                // DEBUG FEEDBACK
                debugOut("Escaped quicksand");
         
                // result 3 - escaped from the quicksand
                move_result = 3;
         
            break;

      
            case 5:
                // sq already searched (onyl used in console version)
                move_result=5;
            break;
      
      
        }
   
        debugOut("Move result = " + move_result);
   
        if(move_result==5){
        
            debugOut("That square has already been searched");
      
        }
        else if(move_result>1){

            // Reduce the number of moves
      
            decrementMovesRemaining();
      
            markSquareAsSearched(move_x,move_y);

            if(moves_remaining==0){

                // out of moves
                move_result=4;

            }
      
        }
        else{
      
            // Subtract all remaining moves from the counter, the game is over
            
            decrementMovesRemaining(getMovesRemaining());
        
        }

        // only print the grid if we are in debug mode
        
        if(debug_mode==1){
            printTheGrid(true);
        }
         
   
   
        // Return the result of the move
        
        return move_result;
        
    } //checkMove ENDS








    /* ladyLuck - determines whether you live or die by generating a random 1 or 0 
     *
     */
     
    public int ladyLuck(){
   
        int chance =0;
   
        // iterates for difficulty_level no of times
        // increasing the chance of death at higher difficulty
        // breaking immediately on 1 being returned.
        for(int i=0;i<difficulty_level;i++){
            chance = (int)(Math.random()*2);
                
                //
                debugOut("ladyLuck: count " + i);
            if(chance==1){
                break;
            }
        }   
        // DEBUG FEEDBACK
        debugOut("Calculated chance of survival: " + chance);
   
        return chance;
        
   
    } // ladyLuck ENDS






    /* hideTheTreasue : a SET Method of class Game
     *
     *
     * @param move_x the current move's x coord
     *
     * @return hint
     */

    private void hideTheTreasure(int size_of_grid){

        // Generate the quicksand first
   
        for(int i=0;i<no_of_quicksands;i++){
      
            int sand_x = (int)( Math.random()* size_of_grid );
            int sand_y = (int)( Math.random()* size_of_grid );
      
            // as we do this before hiding the treasure we dont need to check its not the treasure square
      
            treasure_array[sand_x][sand_y]=2;
            debugOut("Hiding sand at " + sand_x + "," + sand_y); 
            
        }
   
        // hide the tresaure
   
        int treasure_x = (int)( Math.random()* size_of_grid );
        int treasure_y = (int)( Math.random()* size_of_grid );
    
        treasure_array[treasure_x][treasure_y] = 1;
   
    
        // output
   
        debugOut("Treasure be at:" + treasure_x + "," + treasure_y);
   
   
        // DEBUG FEEDBACK
   
        //~ printTheGrid(debug);
   
   
   
    } // hideTheTreasure ENDS


    /* Generates a random ( well 1 in 6) insult to display when moves get low
     */


    public String insultMe(){
   
        // if there are a third of the original no of moves left, be a bit rude.
        if(moves_remaining<=((moves_allowed/difficulty_level)/3)){
            String[] insults_array = { "Look, a three headed monkey!",
                                       "Hey, look over there!", 
                                       "I once owned a dog that was smarter than you.",
                                       "This is getting boring",
                                       "Wake me up when you're done'",
                                       "I hope you have a boat ready for a quick escape."};
   
            int seed = (int)(Math.random()*6);
   
            return insults_array[seed] + "\n";
            
        }   
        
        return "";   
   
    };



   /* Renders the grid on the console as one would expect an interface to do
    * This allows interfaces to be written that have a mode for or are exclusively console/terminal based
    *
    * This method is also called by this class when in debug mode
    *
    * @param    debug   boolean     If debug is set to true the output will reveal the location of quicksand and treasure.
    */
     

    private void printTheGrid(boolean debug){
   
        size_of_grid  = treasure_array.length;
   
        // Print out the top row of coords



        for(int y=0;y<size_of_grid;y++){
      
            if( y==0 ){
         
                System.out.print("   " );

            }
            
            System.out.print(" " + y + " ");
      
            if(y==size_of_grid-1){
         
                System.out.print("\n");
         
            }
      
         }
   
        // print a line of roughly the right size
   
        for(int y=0;y<size_of_grid-1;y++){
      
            System.out.print("----");
        }
   
        System.out.print("\n");
   
        // print the grid values
        
      
      
        for(int x=0;x<size_of_grid;x++){
            for(int y=0;y<size_of_grid;y++){   
         
                // if( x ==0 ){
                if( y ==0 ){
            
                    System.out.print(x +"| ");
                }

                // unless we are debugging don't indicate where the treasure and quicksand are
                if( (debug==false)&&(treasure_array[x][y]==1||treasure_array[x][y]==2)){
            
                    System.out.print( " 0 ");
            
                }
                else if( treasure_array[x][y]==5 ){
            
                    System.out.print( " # ");
            
            
                }
                else{
                
                    System.out.print( " " + treasure_array[x][y] + " ");

                }
         
                // if (x == size_of_grid-1){
                if (y == size_of_grid-1){
            
                    System.out.print(" |\n");
            
                }
                
         
            }
      
        }
         
    } // printTheGrid ENDS




    /* Based on the size of the current game grid - Calculates and then searches in, a number of squares 
     * surrounding the move just made.
     *
     * Care is taken to ensure that when the current move is near the edge of the grid, that values that are not
     * in the array representing the game are not searched as this would throwing an index out of bounds error 
     *
     * begin by subtracting the amount of search squares from the current move
     * then for each of these we replicate the test for the y values
     * and example order of processing would therefore be
     *
     *   x = move_x - search_area
     *   x = 4 - 1
     *   x = 3
     *
     * for x = 3 then do the same to y
     *
     *   y = move_x - search_area
     *   y = 4 - 1
     *   y = 3
     * 
     * so the first iteration will search, incrementing until y equals
     * the move plus the search area
     *  e.g. 4 + 1 = 5
     *
     * So, first this will search
     *
     *   3,3 3,4 and 3,5
     *
     * then
     *
     *   4,3 4,4 and 4,5
     *
     * and finally
     * 
     *   5,3 5,4 and 5,5
     * 
     * a non zero value indicates something so the compass direction is calculate based on
     * whether the sq is less than or more than the current move
     * 
     * 
     *         
     * NUMERIC VALUES FOR HINTS
     * 
     * The values for N(1) S(6) E(2) W(4) were chosed so that they could be combined to give a unique 
     * value for NW(1+4=5) NE(1+2=3) SW(6+4=10) and SE(2+6=8)
     * 
     *
     * NW           N          NE
     *
     *      (5)     1   (3)	  
     * W     4	         2     E
     *      (10)    6    (8)
     *
     * SW           S          SE
     * @param search_area an interger representing the number of squares surrounding the move square to search
     * @return hint
     */ 


    public int[] proximityTest( int move_x,int move_y){
        

   
        debugOut("proximityTest called for move " +move_x + "," + move_y 
               + " GRIDSIZE: " + treasure_array.length + " SEARCH_AREA: " + search_area);


        // an 8 key array to store the hints in by setting a 1 or 0
        // to represent a grid as above
        //
        // Buttons are
        // [0]=> NW [1] =>N [2] => NE [3]=>E etc.
        // it is up to the interface class to do something meaningful with this
        // We only need to provide this info once (if at all)) even though
        // proximityTest will return true (and debug output) for identical
        // directions,
               
        int[] hints = new int[9];
      
        
        for(int x=move_x-search_area;x<=move_x+search_area;x++) {

            for(int y=move_y-search_area;y<=move_y+search_area;y++) {

                // Change this to true and recompile for a much more detailed output of what proximity test is up to.
                boolean deep_debug=false;

                if (deep_debug) {
                    debugOut("Considering " + x + "," + y);
                }

                // declaring here will reset the array on each iteration
              

         
                // check that the current calculated x and y vakues are within the range of the game's treasure_array
                // else we would throw an indexArrayOutOfBounds Exception

                
                if ( ( ( x >= 0 ) && ( x < treasure_array.length ) ) && ( ( y >= 0 ) && ( y < treasure_array.length ) ) ) {

                    // Deep_debug output
                    if (deep_debug) {
                        debugOut("proximityTest: scanning sq " + x + "," + y);
                    }
                    
                    if( treasure_array[x][y]>0 && treasure_array[x][y]<5){


                        // N/S value
                        int lat = 0;
                        // E/W value
                        int lon = 0;
                        
                   
                        String hint_x = "";
                        String hint_y = "";

                            
                   
                        if(x>move_x){
                            hint_x = "South";
                            lat=6;
                        }
                   
                        if(x<move_x){
                            hint_x="North";
                            lat=1;
                        }
                           
                   
                        if(y>move_y){
                            hint_y = "East";
                            lon=2;
                        }
    
                        if(y<move_y){
                            hint_y="West";
                            lon=4;
                        }

                        int hint = lat+lon;
                        if(hint>0){
                            debugOut("proximityTest: hint_value " + hint);

                            switch(hint){

                                case 1: // NORTH
                                    hints[1] = 1;
                                break;

                                case 2: // EAST
                                    hints[5] = 1;
                                break;


                                case 3: // NORTHEAST
                                    hints[2] = 1;
                                break;

                                case 4: // WEST
                                    hints[3] = 1;
                                break;

                                case 5: // NORTHWEST
                                    hints[0] = 1;
                                break;

                                case 6: // SOUTH
                                    hints[7] = 1;
                                break;

                                case 8: // SOUTHEAST
                                    hints[8] = 1;
                                break;

                                case 10: // SOUTHWEST
                                    hints[6] = 1;
                                break;


                            }

                        
                        }

                        
                  
                        if (hint_x != "" || hint_y != ""){
                            
                            debugOut("proximityTest : (" + x + "," + y + ") Something Is Nearby! - HINT: " + hint_x + " " + hint_y );
    
                        }
                        
                    
                    
                        
                        
                    }

            
                }
                else {
                    // Only called deep_debug is set to 1 and we are outputting debug information (disabled by default)
                    if (deep_debug) {
                        debugOut("proximityTest : Rejecting move " + x + "," + y);
                    } 

                }
         
            }
      
        }

        return hints;
   
    }  // ProximityTest ENDS

} // Class Game ENDS



