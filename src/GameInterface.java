

/* Game interface
 * Instantiates an instance of class Game and provides a swing based GUI
 *
 *
 *
 */


import java.io.*;
import java.util.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.*; // used for the URL class used in the help panels


class GameInterface extends JFrame implements ActionListener{


    // Interface objects referenced elsewhere in this class's scope
   
    // Configuration Panel 

    private JButton quit_button; 
    private JButton newgame_button; 
    private JTextField moves_remaining_field;
    private JComboBox game_size_chooser;
    
    private JButton help_button;
    private JDialog help_dialog;
    
    // Map Panel 

    private MapPanel map_panel; 
    private JButton button[][];
    private GridLayout game_grid;

    
    // Feedback Panel

    private JTextArea feedback_box;
    private JComboBox difficulty_level_chooser;
    private String welcome_message;
    private JPanel feedback_panel;

    // Hint panel

    private MapPanel hint_panel;
    private JButton hint_button[];
    private String[] hint_button_text = { "NW","N","NE","W","center","E","SW","S","SE"};

    // Instantiate the Game class
    private Game running_game ; 
    
    // Used for updating window elements
    private Container game_window;
    


    /* Constructor for GameInterface.
     * Creates all of the elements for the GUI
     */

    public GameInterface(){
   
        // Create a new game object
        running_game = new Game();
        
        // This will be displayed at the start of every 'new game'
        welcome_message  = "\nYarr. Ye have stolen a treasure map from a band of swarthy pirates and set sail " 
                         + "for Swimp Island where the map says the loot be buried"
                         + "\nCan you find the treasure before the pirates catch up with you?\n" 
                         + "You can only search one map square per day... do be careful out there!";
        
        // DEBUG OUTPUT via Game.debugOut()
        running_game.debugOut("################################################");
        running_game.debugOut("TreasureHuntGameX : New Game Started. Yarr! ");
        running_game.debugOut("################################################");
        
        // Choices for game size, 3 is a good minimum and is in keeping with the original game, 
        // anything larger than 7 and the game becomes overlong 
        
        String grid_size_choices[] = { "3","4","5","6","7"};   // choices for game_grid_size JComboBox

        // Choices for game difficulty
        // this is used as a divider/mulitplyer in calculating (respectively) the number of moves and how much quicksand is
        // hidden in the game grid
        //
        // 1 is the default

        String game_difficulty_choices[] = { "1","2","3"};   // choices for game_grid_size JComboBox

        //////////////////
        // Window settings

        // Set the applications icon (app_image is also used by the help window
        
        ImageIcon app_image = new ImageIcon("images/TreasureHuntGameX_sm.png");

        setIconImage(app_image.getImage());
   
        // This sets the title of the window
        setTitle("Treasure Hunt Game X");

     
        // Ensure that all window managers decorate the help window
        JDialog.setDefaultLookAndFeelDecorated(true);
        
        // The main 'help' window
        //
        // This uses a fixed size, TabbedPane layout
        // The Story, Instructions, Symbols pages all use
        // 
        
        
        help_dialog = new JDialog();
            help_dialog.setTitle("Treasure Hunt Game X Help");
            help_dialog.setIconImage(app_image.getImage());
            help_dialog.setMinimumSize(new Dimension(480,680));
            
            // Keep the window from being resized
            
            help_dialog.setResizable(false);
            
            // pop the help page up in the center of the screen
            help_dialog.setLocationRelativeTo(null);
            
            // Ensure that the window is closed not disposed of
            
            help_dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

            // Use an editor
            
            
            // Instructions Tab
            
            JEditorPane help_pane = new JEditorPane();
            JScrollPane help_scroll = new JScrollPane(help_pane);

                // Attempt to load the help file
            
                try{
                
                    URL help_html = new URL("file", "", "help_files/Instructions.html");
    
                    help_pane.setPage(help_html);
                
                }
                catch(IOException e){
                
                     // DEBUG OUTPUT       
                    running_game.debugOut("Help Panel: cannot load help file" + e);
                
                }
                    
                help_pane.setEditable(false);
                
                help_pane.setMargin(new Insets(5,5,5,5));
                    
                    
                    
            File help_file = new File("help_files/Instructions.html");

            JPanel about_pane = new JPanel();
                about_pane.setBackground(new Color(255,255,255));



            // About Tab

            JLabel title_image = new JLabel(new ImageIcon("images/titlescreen.gif"));

            JTextPane about_text = new JTextPane();
                about_text.setMargin(new Insets(14,7,14,7));  
                about_text.setText("TreasureHuntGameX\n\nA reimagining of the 'classic' TreasureHunt game in Java Swing"
                                  +"\n\nConceived and coded by 9906420 Game Studios\n\nVersion 3.5.3RC3");
                about_text.setEditable(false);
                
            about_pane.add(title_image);
            about_pane.add(about_text);


            // Game Symbols Tab
                
            JTextPane game_symbols_pane = new JTextPane();
                    game_symbols_pane.setEditable(false);
                try{

                    URL symbols_html = new URL("file", "", "help_files/Symbols.html");
    
                    game_symbols_pane.setPage(symbols_html);
                    
                }
                catch(IOException e){
                    // DEBUG OUTPUT        
                    running_game.debugOut("Help Panel: cannot load symbols file" + e);
                }    

            JScrollPane symbols_scroll = new JScrollPane(game_symbols_pane);
               
               
               
            // Game Story Tab

            JTextPane game_story_pane = new JTextPane();
                game_story_pane.setEditable(false);
            
            try{
            
                    URL story_html = new URL("file", "", "help_files/Story.html");
    
                    game_story_pane.setPage(story_html);
                }
                catch(IOException e){
                            
                    // DEBUG OUTPUT
                    running_game.debugOut("Help Panel: cannot load Story.html file" + e);
                }

            JScrollPane game_story_scroll = new JScrollPane(game_story_pane);
            


            // Add elements to tabs
            
            JTabbedPane help_tabbed_pane = new JTabbedPane();
        
                help_tabbed_pane.addTab("The Story", app_image , game_story_scroll, "The backstory - everyone has one!");
                help_tabbed_pane.addTab("Instructions", app_image , help_scroll, "How to play the game");
                help_tabbed_pane.addTab("Game Symbols", app_image, symbols_scroll ,  "Symbols used within the game");
                help_tabbed_pane.addTab("About", app_image , about_pane, "Game info and credits");
            
                
            help_dialog.add(help_tabbed_pane);   



        ////////////////////////////////////////////////////////////////////////////////////////////////
        // Configuration Panel for user settings etc
        
        JPanel configuration_panel = new JPanel();
               configuration_panel.setLayout(new GridLayout(0,3)); 
            
            JLabel moves_remaining_label = new JLabel(" Days Remaining: ");
                  
            
            JLabel game_size_chooser_label = new JLabel(" Game size: ");
                  


            JLabel difficulty_level_chooser_label = new JLabel(" Game difficulty: " );
            // ComboBox dropdown menu to select game size
            
            game_size_chooser = new JComboBox(grid_size_choices);

                
                // set selected to the selected size.
                // we must subtract whatever the first grid_size_choices value is so that we can call by index
                 
                game_size_chooser.setSelectedItem(grid_size_choices[running_game.getGridSize()-3]);
                game_size_chooser.addActionListener(this);
                game_size_chooser.setToolTipText("Change the size of the map grid");

            // JRadioButton to choose the game's difficulty

            
            difficulty_level_chooser = new JComboBox(game_difficulty_choices);
                difficulty_level_chooser.setToolTipText("<html>Change the difficulty of the game<br>"
                                                      + "Higher settings will affect how far away the<br> "
                                                      + "Voodoo Compass can sense things and determines<br> "
                                                      + "how many moves are permitted and how many hazards<br>"
                                                      + "there are in game.");
                difficulty_level_chooser.addActionListener(this);
             
                // Set the currently selected difficulty value
                // we must subtract whatever the first game_difficulty_choices value so that we can call by index
                // from the game_difficulty_choices array
                difficulty_level_chooser.setSelectedItem(game_difficulty_choices[running_game.getDifficultyLevel()-1]);
                
            // Quit Button
            
            quit_button = new JButton("Quit");
                quit_button.addActionListener(this);
                quit_button.setFocusPainted(false);
                quit_button.setToolTipText("Exits the game. boooo!");

            // New Game Button
            
            newgame_button = new JButton("New Game");
                newgame_button.addActionListener(this);
                newgame_button.setFocusPainted(false);
                newgame_button.setToolTipText("<html>Start a new game with the chosen settings."
                                            + "<br>All current game progress will be lost!</html>");

            // Display how many moves (days) remain before the pirates arrive
            
            moves_remaining_field = new JTextField();
                moves_remaining_field.setEditable(false);
                moves_remaining_field.setForeground(new Color(255,255,255));
                moves_remaining_field.setBackground(new Color(0,0,0));
                moves_remaining_field.setFont(new Font("Lucida Sans", Font.PLAIN, 12));
                moves_remaining_field.setToolTipText("The number of days before the pirates arrive");
            
            // help button
            
            help_button = new JButton("Help");
                help_button.addActionListener(this);
            
            // Set the text in the moves_remaining_field

            updateMovesRemaining();
            
        // Combine elements in the 3x3 configuration Grid Layout

        // Row 1
        configuration_panel.add(difficulty_level_chooser_label);
        configuration_panel.add(difficulty_level_chooser);
        configuration_panel.add(quit_button);

        // Row 2
        configuration_panel.add(game_size_chooser_label);
        configuration_panel.add(game_size_chooser);
        configuration_panel.add(newgame_button);

        // Row 3
        configuration_panel.add(moves_remaining_label);
        configuration_panel.add(moves_remaining_field);
        configuration_panel.add(help_button);        


        //////////////////////////////////////////////////////////////////////////////////////////
        // Map Panel

        map_panel = new MapPanel("images/island_clouds5.gif");
            

            //~~ game_grid = new GridLayout(0,running_game.getGridSize());

            paintGrid();
            makeGridButtons();
 

        //////////////////////////////////////////////////////////////////////////////////////////
        // Feed back panel that contains a JScrollPane with a JText area within
   
        feedback_panel = new JPanel();

        // FlowLayout is default but this is allows us to center         

        FlowLayout feedback_flow = new FlowLayout(FlowLayout.CENTER,2,0);
            feedback_panel.setLayout(feedback_flow);
            
            feedback_panel.setBackground(new Color(0,0,0));
   
         
            feedback_box = new JTextArea();
                feedback_box.setFont(new Font("Lucida Sans", Font.PLAIN, 13));
                feedback_box.setLineWrap(true);
                feedback_box.setWrapStyleWord(true);
                feedback_box.setBackground(new Color(0,0,0));
                feedback_box.setForeground(Color.white);
                feedback_box.setEditable(false);
                feedback_box.setRows(6);
                // feedback_box.setSize(new Dimension(640,90));
                // feedback_box.setMaximumSize(new Dimension(640,90));
                // Set some margins in the textarea
                feedback_box.setMargin(new Insets(2,1,1,5)); 
            

        JScrollPane feedback_scroll = new JScrollPane(feedback_box); 
            
            // disable auto scrolling
            feedback_scroll.setAutoscrolls(false);
            // remove border
            feedback_scroll.setBorder(null);
            // Disable scrollbars
            feedback_scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
            feedback_scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            //~ feedback_scroll.setPreferredSize(new Dimension(645,100));
            feedback_scroll.setPreferredSize(new Dimension(520,100));
        
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        // Hint box to display the proximity test result hints
      
        hint_panel = new MapPanel("images/compass_bw_new.gif");
        
        // The hint box uses a 3x3 grid layout to represent the compass
        // points and a center square to accomodate the voodoo eye
        GridLayout hint_grid = new GridLayout(0,3);
        
            hint_panel.setLayout(hint_grid);
            
            hint_panel.setSize(new Dimension(100,100));
            
            // We use JButton to allow for these to be used for the unimplemented
            // 'cheat entry' feature to be added later by adding an ActionListener
            
            // Create an array of buttons
            hint_button = new JButton[9];
            
            // DEBUG OUTPUT
            running_game.debugOut("Found " + hint_button_text.length + " hint buttons to make");
                
                // configure the buttons and add to the hint panel
                for( int i=0;i<hint_button_text.length; i++){
                    
                    running_game.debugOut("Attempting to create hint_button[" + i + "]");
                    hint_button[i] = new JButton();
                    hint_panel.add(hint_button[i]);
                    hint_button[i].setMargin(new Insets(0,0,0,0)); 
                    hint_button[i].setOpaque(true);
                    hint_button[i].setContentAreaFilled(false);
                    hint_button[i].setBorderPainted(false);
                    hint_button[i].setFocusPainted(false);
                    hint_button[i].setText(hint_button_text[i]);

                }
                
                // button 4 is the centre button so we treat this differently               
                
                hint_button[4].setIcon(new ImageIcon("images/eye_closed.gif"));
                hint_button[4].setText("");
                
                // reset the hint buttons
                // as this method sets the images
                
                resetHintButtons();
    
        feedback_panel.add(feedback_scroll);

        feedback_panel.add(hint_panel);
        
        // Get the root window

        game_window = getContentPane();
      
        // Place the JPanels into position in the JFrame
        
        game_window.add(configuration_panel, BorderLayout.PAGE_START);
        game_window.add(map_panel, BorderLayout.CENTER);
        game_window.add(feedback_panel, BorderLayout.PAGE_END);

        // User notification

        feedback_box.append(welcome_message);
        feedback_box.append("You have " + running_game.getMovesRemaining() + " days before the pirates arrive\n");
   
      }

    /* ActionEvent Listener 
     * 
     * 
     */ 

    public void actionPerformed(ActionEvent action){
   
        running_game.debugOut("Action called: " + action.getSource() );


        // Quit button

        if(action.getSource() == quit_button){

             // Double check that quitting was the intention - it's (an annoying) software convention
             
             int choice = JOptionPane.showConfirmDialog(map_panel, "Are you sure you want to quit this awesome game?",
                                                                   "Sure you want to quit?",JOptionPane.YES_NO_OPTION );
            // DEBUG OUTPUT
            running_game.debugOut("quit requested");

            if(choice==0){

                System.exit(0);

            }
         
            // DEBUG OUTPUT
            running_game.debugOut("Quit button event received, exiting");
 

        }

        // New Game

        if( action.getSource() == newgame_button ) {


            running_game.debugOut("New Game button event received");

            map_panel.removeAll();

            // Start a new Game
            running_game = new Game();

            
            // Call our method which will generate the required gridlayout and buttons
            paintGrid();
            
            // make new set of buttons
            makeGridButtons();

            // Refresh the displayed elements 
            map_panel.revalidate();
            map_panel.repaint();
            feedback_box.revalidate();
            feedback_box.repaint();
            feedback_box.setText("\n" + welcome_message);
            feedback_box.append("\nYou have " + running_game.getMovesRemaining() + " days before the pirates arrive\n");
  
            updateMovesRemaining();
            
            // Set the hint buttons back to their default state
            resetHintButtons();
        }
       
        // Change of game size

        if( action.getSource() == game_size_chooser ) {
         
            running_game.setGridSize(Integer.parseInt((String)game_size_chooser.getSelectedItem()));

            running_game.debugOut("Game size change event received. Size " + running_game.getGridSize() + " chosen.");
            
            // Write to the config file immediately
            running_game.saveConfig();

        }

        // Change of game difficulty

        if( action.getSource() == difficulty_level_chooser ) {
        
            running_game.setDifficultyLevel(Integer.parseInt((String)difficulty_level_chooser.getSelectedItem()));
            
            running_game.debugOut("Game difficulty change event received. Level " 
                                + running_game.getDifficultyLevel() + " chosen.");
            
            // Write to the config file immediately
            running_game.saveConfig();


        }

        
       
        // Use getActionCommand on the buttons as they have been created with a move value as their action command
        
        if(action.getActionCommand()=="move"){

            // Convert the move recived (coords) into a String
            String move_received =((JButton) action.getSource()).getName();    

            running_game.debugOut(" MOVE DETECTED: " + move_received);
      
            // The value received will be a
            // String containing the x,y pair from the
            // map grid...  hmmm, x,y pair... deja vu!
          
            String[] move = move_received.split(",");
            
            int move_x = Integer.parseInt(move[0]);
            int move_y = Integer.parseInt(move[1]);
      
            

            int move_result = running_game.checkMove(move_x,move_y);

            // No more moves means game over
         
            // Update the moves remaining field
            updateMovesRemaining();

            // Use the handleMove() method
            handleMove(move_x,move_y,move_result);


        }
   
   
   
        // Help Dialog 

        if( action.getSource() == help_button ) {

            help_dialog.setVisible(true);

        }
   
    
    }


    /*Proiximity Event Handler
     * takes the move that was made and performs a search in the surrounding squares for
     * hazards and the treasure
     * 
     */

    private void handleProximity(int move_x, int move_y){

        int[] proximity_test_results = running_game.proximityTest(move_x,move_y);

        
        // if(proximity_test_results.length>0){

        int hint_trigger = 0;
            for(int i=0;i<proximity_test_results.length;i++){

                // Set JButton in hint panel to show its value
                //button

                // Skip the centre button
                if(i!=4){

                    if(proximity_test_results[i]>0){
                        running_game.debugOut("Enabling hint button " + i );
                        hint_button[i].setForeground(new Color( 255,255,59));
                        hint_button[i].setToolTipText("<html>Searching to the " + hint_button_text[i] 
                                                    + " of here may prove fruitful..."
                                                    + "<br>or be the last act ye perform upon this earth</html>");
                        hint_button[i].setFont(new Font("Lucida Sans", Font.BOLD, 17));

                        hint_trigger=1;
                    }else{
                        running_game.debugOut("Disabling hint button " + i );
                        hint_button[i].setForeground(Color.black);
                        hint_button[i].setToolTipText("");
                        hint_button[i].setFont(new Font("Lucida Sans", Font.BOLD, 1));

                    }

                }
            }
        feedback_box.append("");
        if(hint_trigger==1){

        feedback_box.append("The Voodoo Compass has detected something nearby... what could it be \n");
        hint_button[4].setToolTipText("<html>These directions might be useful<br>"
                                    + "Or they may lead you into terrible peril");
        hint_button[4].setIcon(new ImageIcon("images/eye_blink.gif"));
        }
        else{
        // Set the eye image back to it's default state
        hint_button[4].setIcon(new ImageIcon("images/eye_closed.gif"));

        }

    }

    /* Disables all of the currently displayed buttons
    *
    *
    */

    private void disableButtons(){

        for(int x=0;x<running_game.getGridSize();x++){

            for(int y=0;y<running_game.getGridSize();y++){

                button[x][y].setEnabled(false);

            }

        }


    }

    /* Resets the hint buttons to their default state
     *
     */

     private void resetHintButtons(){

        for(int i=0;i<9;i++){


            hint_button[i].setForeground(Color.black);
            hint_button[i].setFont(new Font("Lucida Sans", Font.BOLD, 1));


        }
        
        if(running_game.getSearchArea()==0){
            hint_button[4].setToolTipText("<html>The Voodoo Compass is disabled at this difficulty level, "
                                        + "either use your own voodoo powers or"
                                        + "<br>Make the grid larger and/or decrease the difficulty to reactivate the Voodoo Compass"
                                        + "<br>Though to be honest you're probably better off without it...");
            hint_button[4].setEnabled(false);
        }   
        else{
            hint_button[4].setEnabled(true);

            hint_button[4].setToolTipText("<html>This is the voodoo compass <br> It can detect nearby gold.... mutter mutter "
                                        + "... and quicksand and rip tides <br>The Voodoo Compass will give you hints as you play, "
                                        + " these may or may be helpful in your search<br>By the way... "
                                        + "The Compass will not help you write a letter...</html>");
            
        }
        running_game.debugOut("resetHintButtons: Resetting center hint button image");
        hint_button[4].setIcon(new ImageIcon("images/eye_closed.gif"));


     }

    /* If the size of the grid is greater than the int value specified: 
     * Checks to see whether the grid ref provided is at the edge of the map
     * This allows a different hazard to be used, death by drowning at the edge
     * or quicksand when on land
     */


    private boolean isEdgeOfMap(int x, int y){
        
        boolean is_edge = false;
            
            // Only continue if the map grid is large enough
            if(running_game.getGridSize()>=5){    
                
               // detect edge squares 
               if(x==0||x==(running_game.getGridSize()-1)||y==0|| y==(running_game.getGridSize()-1)){
                    
                    // DEBUG OUTPUT
                    running_game.debugOut("isEdgeOfMap: Edge of map detected for hazard at " + x + "," + y);
                    
                is_edge=true;
                
                }  
            
            }
        
        return is_edge;
        
    }

 
    /* Adjusts the interface according to the move received 
     * this is passed the result from Game.checkMove()
     */

    private void handleMove(int x,int y,int move_result ){

        button[x][y].setOpaque(false);

        // Disable the button that was pressed.

        button[x][y].setBorderPainted(false);
        
        // move_result corresponds to the output of the checkMove method in the Game class.
        // 2 on nothing, 0 for treasure, 1 for death by quicksand, 3 for escape from quicksand;

        // scroll the feedback_box down by 7 lines for a neater appearance, clearing the previous output
        // from view but retaining it in the message box (scrolling still works despite scrollbars disabled)
        feedback_box.append("\n\n\n\n\n\n\n");
        if(move_result == 0){
    
            // Treasure found

            button[x][y].setToolTipText("<html>This be where the treasure was found! Well done.</html>");
            button[x][y].setDisabledIcon(new ImageIcon("images/treasure_chest_anim.gif"));
            feedback_box.append("CONGRATULATIONS!\n\nYou found the treasure, now move swiftly and"
                               +" 'Escape from Swimp Island!'\n");  

        }

        if(move_result == 1){

            // Died by hazard
            
                       
            // Unless the map square is at the edge
            if(isEdgeOfMap(x,y)==true){
                
                button[x][y].setToolTipText("<html>You got caught in a rip tide whilst searching the seabed..."
                                          + " and drowned.<br>Better luck next time eh!</html>");
                feedback_box.append("\nOh no! - You drown, caught in a rip tide, whilst searching the seabed."
                                  + " The sharks will eat well today!\nGAME OVER\n");  
                button[x][y].setDisabledIcon(new ImageIcon("images/hand_trans.gif"));
            }else{
                
                button[x][y].setToolTipText("<html>You fell in quicksand here... and died."
                                          + "<br>Better luck next time eh!</html>");
                feedback_box.append("\nOoops! - You fall in quicksand and die. What a plonker!\n\nGAME OVER\n");   
                button[x][y].setDisabledIcon(new ImageIcon("images/quicksand_swirl.gif"));
   
            }
           
           
            
        }

        if(move_result == 2){

            // Nothing to see here
            button[x][y].setToolTipText("<html>Arr, you've already searched here."
                                      + "<br> You even went to the trouble of leaving a flag!<br>"
                                      + "HINT: Choose somewhere else to dig... sheesh.</html>");

            button[x][y].setDisabledIcon(new ImageIcon("images/goodguy_flag_icon_92.gif"));

            feedback_box.append("\nYou search from dawn until dusk but no treasure do you find\n");


        }
        

        if(move_result == 3){

        // Escaped from hazard... lucky b*****d
            
            if(isEdgeOfMap(x,y)==true){

                button[x][y].setToolTipText("<html>You almost drowned here <br> Best to stay away and choose somewhere else to"
                                      + " search...</html>");

                button[x][y].setDisabledIcon(new ImageIcon("images/life_ring.gif"));

                feedback_box.append("\nOh Dear! - You almost drown searching the seabed. \n"
                              + " Having used up all your luck for the day, you find no treasure.\n");  

            
            }
            else{
                button[x][y].setToolTipText("<html>You fell in quicksand here and survived."
                                          + "<br> Best to stay away and choose somewhere else to"
                                          + " search... s'no treasure here matey</html>");

                button[x][y].setDisabledIcon(new ImageIcon("images/quicksand_icon.gif"));

                feedback_box.append("\nIsn't that a pip! - You fall in quicksand but manage to pull yourself free. \n"
                              + "Thought having depleted your luck for the day, you find no treasure.\n");  

            
            }
            
        }

         if(move_result == 4){

            // Killed by the pirates
            
            button[x][y].setToolTipText("<html>The Pirates arrive and kill you before you can find the treasure.<br>"
                                      + " At least the worms won't go hungry'</html>");

            feedback_box.append("\nYou took too long, the pirates arrive and turn you into worm food. \n\nGAME OVER\n");  

            button[x][y].setDisabledIcon(new ImageIcon("images/pirate_flag_icon_92.gif"));

            button[x][y].setBackground(new Color(0,0,255));
            
        }

        if(move_result == 2 || move_result == 3){

            handleProximity(x,y);

            feedback_box.append("Only " + running_game.getMovesRemaining() + " days left! ");

            if(running_game.getMovesRemaining()< running_game.getMovesAllowed()/2){
                           
                feedback_box.append("\n" + running_game.insultMe());
            }

            
        }
        

        button[x][y].setEnabled(false);
        running_game.debugOut("Changing button state for " + x + "," + y );

        // If we have run out of moves disable all buttons to prevent further moves being taken

        if(running_game.getMovesRemaining()==0){

                // GAME OVER
                
                // Disable all the grid buttons to prevent further moves
                disableButtons( );

        }

    }


   /* This renders the grid buttons for the map,
    * this is encapsulated in a method as different map sizes
    * require different amounts of buttons
    */


    private void makeGridButtons(){
      
        int size_of_grid = running_game.getGridSize();
        running_game.debugOut("makegridbuttons has grid size: " + size_of_grid);

      
        button = new JButton[size_of_grid][size_of_grid];
      
      
        for(int x=0;x<size_of_grid;x++) {
            for(int y=0;y<size_of_grid;y++) {
                String button_value = x +"," + y;
              
                
                // We set a blank image as the defualt as without this
                // the layout gets messed up when changing the image 
                // in response to moves
                
                Icon blank_image = new ImageIcon("images/blank.gif");


                button[x][y] = new JButton();

                button[x][y].setIcon(blank_image);
                
                button[x][y].setName(button_value);
                button[x][y].setToolTipText("Yarr, be the treasure be here? Click to dig and find out.");

                button[x][y].setOpaque(true);
                button[x][y].setContentAreaFilled(false);
                
                button[x][y].setActionCommand("move");
                button[x][y].setSize(new Dimension(640/size_of_grid,400/size_of_grid));
                button[x][y].addActionListener(this);
                
                // stop the highlight jumping to the next jbutton
                button[x][y].setFocusPainted(false);

                if(running_game.getDifficultyLevel()>=3){
            
                    button[x][y].setBorderPainted(false);
                
                }

                button[x][y].setHorizontalAlignment(SwingConstants.CENTER);
                button[x][y].setVerticalAlignment(SwingConstants.CENTER);
                button[x][y].setRolloverEnabled(true);
    
    

                button[x][y].setRolloverIcon(new ImageIcon("images/little_miner_92.gif"));
                button[x][y].setPressedIcon(new ImageIcon("images/little_miner_92.gif"));
                button[x][y].setDisabledIcon(new ImageIcon());
                button[x][y].setCursor(new Cursor(Cursor.HAND_CURSOR));

                //Debug Output         
                running_game.debugOut("Generated button[" + x + "][" + y + "] Action: 'move' size " + button[x][y].getSize());
                
                // Add the generated button to the map_panel
                map_panel.add(button[x][y]);
         
            }
      
        }
      
    
    }

    /* Genereates the game grid     
     * Encapsulated as a method as this is called on new game action
     */

    private void paintGrid(){


            
        game_grid = new GridLayout(0,running_game.getGridSize());
         
        // ensures the grid panel doesn't exceed the size of the images

        map_panel.setMaximumSize(new Dimension(642,400));
        map_panel.setLayout(game_grid);
        
        
    

    } // method paintGrid ENDS


    /* Updates the number of moves remaining in the game window
     */

    private void updateMovesRemaining(){


        moves_remaining_field.setText(Integer.toString(running_game.getMovesRemaining()));

    } 
    

} // class GameInterface ENDS
