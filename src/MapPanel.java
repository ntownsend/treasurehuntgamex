/* Allows the painting of an image, passed to the constructor as a String, 
 * into a JPanel by overriding the paintComponent method to display the image
 * 
 * Modified from the example at : http://www.coderanch.com/how-to/java/BackgroundImageOnJPanel
 */
 

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.imageio.ImageIO.*; // used to extend JPanel


class MapPanel extends JPanel{
    Image background_image;
  
    public MapPanel(String b_image){
        
        
        try{

            //image = new ImageIcon("island.jpg").getImage();
         //   background_image = new ImageIcon("images/island_clouds5.gif").getImage();
            background_image = new ImageIcon(b_image).getImage();
                    
                
            
        }
        catch (Exception e) {
            /* the  'background_image !=null' check in the overridden paintComponent()
             * means that 'painting' the image is only attemoted if the file exists.
             * 
             */

        }

    }
    
   @Override
   // paints the image into the JPanel
   protected void paintComponent(Graphics g)
      {
      super.paintComponent(g); 
      if (background_image != null)
         g.drawImage(background_image, 0,0,this.getWidth(),this.getHeight(),this);
      }

    

    
}
