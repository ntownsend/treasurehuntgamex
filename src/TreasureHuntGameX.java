/* TreasureHuntGameX
 *
 * A View and Controller style class that instantiates 
 * and controls an instance of Game.class
 *
 * 
 */



import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class TreasureHuntGameX{

    public static void main(String[] args){
       
            // Request the Cross Platform / Metal look and feel be used as default
            // to allow a consistent User Interface on OS X, Linux and Windows.
            //
            // L&F also chosen for reason of looking most like an 'old school' 
            // 8/16 bit era game interface, in fitting with the overall theme 
            // of the game's appearance.
            //
            // Based on the examples at:
            // http://docs.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
            
        try {

            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            
        } 
        catch (UnsupportedLookAndFeelException e) {
        
            e.printStackTrace();
        
        }
        catch (IllegalAccessException e) {
        
            e.printStackTrace();
        
        }
        catch (InstantiationException e) {
           
            e.printStackTrace();
        
        }
        catch (ClassNotFoundException e) {
        
            e.printStackTrace();
        
        }
        
 

        // Load the Swing application so that it runs on the Event Dispatch Thread
        // using a modified version examples retrieved from
        // http://docs.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
        //
        // Testing with
        //   System.out.println( javax.swing.SwingUtilities.isEventDispatchThread());
        // in the GameInterface class (line: returns true indicating that as per 
        // http://docs.oracle.com/javase/7/docs/api/javax/swing/SwingUtilities.html#isEventDispatchThread%28%29
        // and
        // http://docs.oracle.com/javase/tutorial/uiswing/concurrency/dispatch.html
        // this application is running on the Event Dispatch Thread.
        //
        // Without this,  instead calling playGameGUI(); directly, 
        // the JTextArea component used in this application
        // will not update correctly when the append method is used.
        
        SwingUtilities.invokeLater(
            new Runnable() {
                public void run() {
                    playGameGUI();
                }
            });


        // Commented to allow the thread safe call above
        // playGameGUI();
    
   } // main ENDS

    /* Creates an instance of GameinterFace which in turn will create an
     * instance of Game (Model) thus creating a running game with 
     * an interface (View+Controller)
     * 
     */

   public static void playGameGUI(){
        
        // Ensures that Window Managers do not override the window decoration appearance
        // and that tiling window managers are requested to decorate the window.
        // Omitting this caused issues on the developers debian machine using 'awesome wm'
        // window manager
        
        JFrame.setDefaultLookAndFeelDecorated(true);

        GameInterface frame = new GameInterface();

        // Set the main window's size and disable resizing
        // to give us full control over the game interface        
        
        frame.setSize(640,620);
        frame.setResizable(false);
           
        // Set the close button to System.exit on click instead of disposing
        // and leaving the game running with no interface
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Display the window
        
        frame.setVisible(true);
    
    }

} // class TreasureHuntGameX ENDS